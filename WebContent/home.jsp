<%@ page language="java" contentType="text/html; charset=UTF-8"
pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page isELIgnored="false"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
	<title>ホーム画面</title>
	<link href="./css/style.css" rel="stylesheet" type="text/css">
	</head>
	<body>

	<div class="main-contents">
			<a href="message">新規投稿</a>
			<a href="management">ユーザー管理</a>
			<a href="logout">ログアウト</a>

			<form action = "./" method = "get">
				<input name = "startDate" type = "date" value = "<c:out value="${ startDate }"/>">～<input name = "endDate" type = "date" value = "<c:out value="${ endDate }"/>"><br>
				<label for = "category">カテゴリー検索</label>
				<input name = "category" id = "category" value = "<c:out value="${ category }"/>">
				<input type = "submit" value = "絞り込み">
			</form>

				<c:if test = "${ not empty errorMessages }">
						<div class = "errorMessages">
							<ul>
								<c:forEach items= "${ errorMessages }" var = "errorMessage">
								<li><c:out value= "${ errorMessage }" />
								</c:forEach>
							</ul>
						</div>
							<c:remove var = "errorMessages" scope = "session"/>
				</c:if>
	</div>

	<div class="messages">
		<c:forEach items="${ messages }" var="message">
			<div class="message">
			<div class="name">
				<span class="name"><c:out value="${ message.name }" /></span>
			</div>
			<div class="title">件名：<c:out value="${ message.title }" /></div>
			<div class="category">カテゴリ：<c:out value="${ message.category }" /></div>
			<div class="text">本文：<pre><c:out value="${ message.text }" /></pre></div>
			<div class="date"><fmt:formatDate value="${ message.createdDate }" pattern="yyyy/MM/dd HH:mm:ss" /></div>
			<c:if test = "${ loginUser.id == message.userId }">
				<form action = "deleteMessage" >
					<input name = "messageId" value = "${ message.id }" id  = "id" type = hidden>
					<input type="submit" value="削除" onClick = "confirmMessage(); return false;">
					<script> function confirmMessage(){
						if(window.confirm('投稿を削除してよろしいですか？')){ // 確認ダイアログを表示
							document.kakuninn().submit();
							return true; // 「OK」時は送信を実行
						}
							else{ // 「キャンセル」時の処理
								return false; // 送信を中止
							}
					}</script>
				</form>
			</c:if>
	</div>


			<div class = "comments">
				<c:forEach items="${ comments }" var="comment">
					<c:if test = "${ comment.messageId == message.id }">
						<div class = "name">
						<span class="name">名前：<c:out value="${ comment.name }" /></span>
						</div>
						<div class = "comment">コメント：<pre><c:out value="${ comment.text }" /></pre></div>
						<div class="date"><fmt:formatDate value="${ comment.createdDate }" pattern="yyyy/MM/dd HH:mm:ss" /></div>
							<c:if test = "${ loginUser.id == comment.userId }">
								<form action = "deleteComment" >
									<input name = "commentId" value = "${ comment.id }" id  = "id" type = hidden>
									<input type="submit" value="削除" onClick = "confirmComment(); return false;">
										<script> function confirmComment(){
											if(window.confirm('コメントを削除してよろしいですか？')){ // 確認ダイアログを表示
												document.kakuninn().submit();
												return true; // 「OK」時は送信を実行
											}
											else{ // 「キャンセル」時の処理
												return false; // 送信を中止
											}
										}</script>
								</form>
							</c:if>
					</c:if>
				</c:forEach>

	<form action = "comment" method = "post">
		<input name = "id"  value = "${ message.id }" id  = "id" type = hidden>
		<textarea name="text" id = "text" cols="50" rows="5" class="comment-box"></textarea><br/>
		<input type="submit" value="投稿">
	</form>

	</div>
		</c:forEach>
</div>
		<div class="copyright"> Copyright(c)Kazumichi Miyauchi</div>

</body>
</html>