<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page isELIgnored="false"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
	<title>ログイン</title>
	<link href="./css/style.css" rel="stylesheet" type="text/css">
	</head>
	<body>

	<div class = "main-contest">

		<c:if test = "${ not empty errorMessages }">
			<div class = "errorMessages">
				<ul>
					<c:forEach items= "${ errorMessages }" var = "errorMessage">
						<li><c:out value= "${ errorMessage }" />
					</c:forEach>
				</ul>
			</div>
		</c:if>

		<form action = "login" method = "post"><br />
			<label for = "account">アカウント</label>
			<input name = "account" id = "account" value = "<c:out value="${ account }"/>"/> <br />

			<label for = "password">パスワード</label>
			<input name = "password" type = "password" id = "password"/> <br />

			<input type = "submit" value = "ログイン" /> <br />
		</form>

		<div class = "copyright"> Copyright(c)Kazumichi Miyauchi</div>
	</div>

</body>
</html>