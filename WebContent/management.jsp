<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page isELIgnored="false"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<title>ユーザー一覧</title>
<link href="./css/style.css" rel="stylesheet" type="text/css">
</head>
<body>
		<div class = "main-header">
			<form action = "home" method = "post">
				<a href = "./">ホーム</a>
				<a href = "signup">ユーザー新規登録</a>
			</form>
		</div>

		<div class="main-contents">
			<c:forEach items="${usersList}" var="userBranchDepartment">

				<div class="account">アカウント:<c:out value="${ userBranchDepartment.account }" /></div>
				<div class="name">名前:<c:out value="${ userBranchDepartment.name }" /></div>
				<div class="branch_name">支社:<c:out value="${ userBranchDepartment.branchName }" /></div>
				<div class="department_name">部署:<c:out value="${ userBranchDepartment.departmentName }" /></div>
				<div class="is_stopped">アカウント復活停止状態:<c:out value="${ userBranchDepartment.isStopped }" /></div>

				<form action = "setting" method = "get">
					<input name = "id"  value = "${ userBranchDepartment.id }" id  = "id" type = hidden>
					<div class="date"><fmt:formatDate value="${ userBranchDepartment.createdDate }" pattern="yyyy/MM/dd HH:mm:ss" /></div>
					<input type = "submit" value = "編集">
				</form>


				<form action = "stop" method = "post">
					<input name = "id"  value = "${ userBranchDepartment.id }" id  = "id" type = hidden>
					<c:if test = "${loginUser.id != userBranchDepartment.id }">
						<c:if test = "${ userBranchDepartment.isStopped == 0 }">
							<input name = "is_stopped"  value = "1" id  = "is_stopped" type = hidden>
							<input type = "submit" value = "停止" onClick = "confirmLive(); return false;">

							<script> function confirmLive(){
										if(window.confirm('停止させてよろしいですか？')){ // 確認ダイアログを表示
											document.kakuninn().submit();
											return true; // 「OK」時は送信を実行
										}
										else{ // 「キャンセル」時の処理
											return false; // 送信を中止
										}
							}</script>
						</c:if>
					</c:if>

					<c:if test = "${ userBranchDepartment.isStopped == 1 }">
						<input name = "is_stopped"  value = "0" id  = "is_stopped" type = hidden>
						<input type = "submit" value = "復活" onClick = "ConfirmBan; return false;">
						<script> function confirmBan(){
									if(window.confirm('復活させてよろしいですか？')){ // 確認ダイアログを表示
										document.kakuninn().submit();
										return true; // 「OK」時は送信を実行
									}
									else{ // 「キャンセル」時の処理
										return false; // 送信を中止
									}
						}</script>
					</c:if>
				</form>
			</c:forEach>

		</div>
		<div class="copyright"> Copyright(c)Kazumichi Miyauchi</div>

</body>
</html>