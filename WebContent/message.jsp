<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page isELIgnored="false"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>新規投稿画面</title>
</head>
<body>
<div class = "main-contents">
	<a href ="./">ホーム</a>

		<c:if test = "${not empty errorMessages }">
			<div class = "errorMessages">
				<ul>
					<c:forEach items = "${ errorMessages }" var = "errorMessage">
						<li><c:out value = "${ errorMessage }"></c:out>
					</c:forEach>
				</ul>
			</div>
		</c:if>

	<form action="message" method="post">

		<label for = "title">タイトル</label>
		<input name = "title" id  = "title"  value = "<c:out value="${ title }"/>"><br/>

		<label for = "category">カテゴリー</label>
		<input name = "category" id  = "category" value = "<c:out value="${ category }"/>" ><br/>

		<label for = "text">投稿内容</label>
		<textarea name="text" id = "text" cols="100" rows="5" class="text-box" ><c:out value="${ text }"/></textarea>
			<br />
		<input type="submit" value="投稿">
	</form>
</div>
</body>
</html>