<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page isELIgnored="false"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<title>ユーザー編集</title>
<link href="./css/style.css" rel="stylesheet" type="text/css">
</head>
<body>

<div class = "main-header">
<a href = "management">ユーザー管理</a>

<div class = "main-contents">

	<c:if test = "${not empty errorMessages }">
			<div class = "errorMessages">
				<ul>
					<c:forEach items = "${ errorMessages }" var = "errorMessage">
						<li><c:out value = "${ errorMessage }"></c:out>
					</c:forEach>
				</ul>
			</div>
	</c:if>

	<form action = "setting" method = "post" >

		<input name = "id"  value = "${ user.id }" id  = "id" type = hidden>

		<label for = "account">アカウント</label>
		<input name = "account" value = "${ user.account }" id  = "account"><br/>

		<label for = "password">パスワード</label>
		<input type="password" name="password" id="password"><br/>

		<label for="passwordConfirm">確認用パスワード</label>
		<input type="password" name="passwordConfirm" id="passwordConfirm"><br/>

		<label for = "name">名前</label>
		<input name = "name" value = "${ user.name }" id = "name"><br/>

		<select name = "branchId">
			<c:forEach items="${ branches }" var="branch">
				<c:if test = "${ branch.id != user.branchId }">
					<option value = "${ branch.id }">${ branch.name}</option>
				</c:if>

					<c:if test = "${ branch.id == user.branchId }">
						<option value = "${ branch.id }" selected>${ branch.name }</option>
					</c:if>
			</c:forEach>
		</select>

		<select name = "departmentId">
			<c:forEach items="${ departments }" var="department">
						<c:if test = "${department.id != user.departmentId }">
							<option value = "${ department.id }">${ department.name}</option>
						</c:if>

				<c:if test = "${department.id == user.departmentId }">
					<option value = "${ department.id }" selected>${ department.name}</option>
				</c:if>
			</c:forEach>
		</select>

		<input type = "submit" value = "更新">
	</form>


</div>

</div>

</body>
</html>