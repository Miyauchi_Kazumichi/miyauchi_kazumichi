<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page isELIgnored="false"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ユーザー登録</title>
</head>
<body>
<div class = "main-contents">
	<div  class = "header">
		<a href = "./">ユーザー管理</a><br/>

		<c:if test = "${ not empty errorMessages }">
			<div class = "errorMessages">
				<ul>
					<c:forEach items = "${ errorMessages }" var = "errorMessage">
						<li><c:out value = "${ errorMessage }"></c:out>
					</c:forEach>
				</ul>
		</div>
		</c:if>

	<form action = "signup" method = "post"><br/>
		<label for = "account">アカウント</label>
		<input name = "account" id  = "account" value = "<c:out value="${ account }"/>"><br/>

		<label for = "password">パスワード</label>
		<input name = "password" type = "password" id = "password"><br/>

		<label for="passwordConfirm">確認用パスワード</label>
		<input type="password" name="passwordConfirm" id="passwordConfirm"><br/>

		<label for = "name">名前</label>
		<input name = "name" id = "name" value = "<c:out value="${ name }"/>"><br/>

		<select name = "branchId">
			<c:forEach items="${ branches }" var="branch">
						<c:if test = "${ branchId != branch.id }">
							<option value = "${ branch.id }">${ branch.name }</option>
						</c:if>

				<c:if test = "${ branchId == branch.id}">
					<option value = "${ branch.id }" selected>${ branch.name }</option>
				</c:if>
			</c:forEach>
		</select>

		<select name = "departmentId">
			<c:forEach items="${ departments }" var="department">
				<c:if test = "${ departmentId != department.id }">
					<option value = "${ department.id }">${ department.name }</option>
				</c:if>

				<c:if test = "${ departmentId == department.id }">
					<option value = "${ department.id }" selected>${ department.name }</option>
				</c:if>
			</c:forEach>
		</select>


		<input type ="submit" value = "登録"><br/>
	</form>
			<div class = "copyright">Copyright(C) Kazumichi Miyauchi</div>
	</div>
</div>

</body>
</html>
