package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Comment;
import beans.Message;
import beans.User;
import service.CommentService;


@WebServlet(urlPatterns = {"/comment"})
public class CommentServlet extends HttpServlet{

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)throws IOException, ServletException{

		request.getRequestDispatcher("/comment.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		HttpSession session = request.getSession();
		String text = request.getParameter("text");
		List<String> errorMessages = new ArrayList<String>();

		//jspで渡されたmessage.idを受け取るため
		Message message = new Message();
		message.setId(Integer.parseInt(request.getParameter("id")));

		Comment comment = new Comment();
		comment.setText(text);
		comment.setMessageId(message.getId());

		if (!isValid(request, errorMessages)) {
			request.setAttribute("text", text);
			request.setAttribute("errorMessages", errorMessages);
			response.sendRedirect("./");
			return;
		}

		User user = (User) session.getAttribute("loginUser");
		comment.setUserId(user.getId());

		new CommentService().insert(comment);
		response.sendRedirect("./");
	}

	private boolean isValid(HttpServletRequest request, List<String> errorMessages) {

		String text = request.getParameter("text");

		if(500 < text.length()) {
			errorMessages.add("コメントは500文字以下で入力してください");
		}
		if(StringUtils.isBlank(text)) {
			errorMessages.add("コメントを入力してください");
		}
		if (errorMessages.size() != 0) {
			return false;
		}
		return true;
	}

}
