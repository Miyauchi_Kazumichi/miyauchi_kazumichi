package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import service.MessageService;

@WebServlet(urlPatterns = {"/deleteMessage"})
public class DeleteMessageServlet extends HttpServlet{

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException{

		new MessageService().delete(Integer.parseInt(request.getParameter("messageId")));

		request.getRequestDispatcher("./").forward(request, response);
	}

}