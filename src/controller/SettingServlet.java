package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import beans.Branch;
import beans.Department;
import beans.User;
import exception.NoRowsUpdatedRuntimeException;
import service.BranchService;
import service.DepartmentService;
import service.UserService;

@WebServlet(urlPatterns = { "/setting" })
public class SettingServlet extends HttpServlet{

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		User user = new UserService().select(Integer.parseInt(request.getParameter("id")));
		request.setAttribute("user", user);

		List<Branch> branchName = new BranchService().select();
		request.setAttribute("branches", branchName);

		List<Department> departmentName = new DepartmentService().select();
		request.setAttribute("departments", departmentName);

		request.getRequestDispatcher("setting.jsp").forward(request, response);
	}
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		List<Branch> branchName = new BranchService().select();
		List<Department> departmentName = new DepartmentService().select();
		List<String> errorMessages = new ArrayList<String>();

		User user = getUser(request);
		if (isValid(request, errorMessages)) {
			try {
				new UserService().update(user);
			} catch (NoRowsUpdatedRuntimeException e) {
				errorMessages.add("他の人によって更新されています。最新のデータを表示しました。データを確認してください。");
			}


		}

			if (errorMessages.size() != 0) {
			request.setAttribute("branches", branchName);
			request.setAttribute("departments", departmentName);

			request.setAttribute("errorMessages", errorMessages);
			request.setAttribute("user", user);
			request.getRequestDispatcher("setting.jsp").forward(request, response);
			return;
		}

		response.sendRedirect("management");
	}


	private User getUser(HttpServletRequest request) throws IOException, ServletException {

		User user = new User();
		user.setId(Integer.parseInt((request.getParameter("id"))));
		user.setAccount(request.getParameter("account"));
		user.setPassword(request.getParameter("password"));
		user.setName(request.getParameter("name"));
		user.setBranchId(Integer.parseInt((request.getParameter("branchId"))));
		user.setDepartmentId(Integer.parseInt((request.getParameter("departmentId"))));
		return user;
	}
	private boolean isValid(HttpServletRequest request, List<String> errorMessages) {

		int id = Integer.parseInt(request.getParameter("id"));
		String account = request.getParameter("account");
		String password = request.getParameter("password");
		String passwordConfirm = request.getParameter("passwordConfirm");
		String name = request.getParameter("name");
		int branchId = Integer.parseInt((request.getParameter("branchId")));
		int departmentId = Integer.parseInt((request.getParameter("departmentId")));

		User differentUser = new UserService().select(account);
		int differentUserId = differentUser.getId();

		if (StringUtils.isEmpty(name)) {
			errorMessages.add("名前を入力してください");
		}
		if(10 < name.length()) {
			errorMessages.add("名前は10文字以下で入力してください");
		}

		if(account.length() < 6) {
			errorMessages.add("アカウントは6文字以上で入力してください");
		}

		if (20 < account.length()) {
			errorMessages.add("アカウント名は20文字以下で入力してください");
		}

		if(!account.matches("^[0-9a-zA-Z]+$")) {
			errorMessages.add("アカウントに使用できない文字が含まれています");
		}

		if(password.length() > 20) {
			errorMessages.add("パスワードは20文字以下で入力してください");
		}

		if(!password.equals(passwordConfirm)) {
			errorMessages.add("入力したパスワードと確認用パスワードが一致しません");
		}

		if(branchId == 3 && (departmentId == 1 || departmentId ==2)) {
			errorMessages.add("支社と部署の組み合わせが間違っています");
		}
		if(branchId == 4 && (departmentId == 1 || departmentId ==2)) {
			errorMessages.add("支社と部署の組み合わせが間違っています");
		}

		if(branchId == 1 && (departmentId == 3 || departmentId ==4)) {
			errorMessages.add("支社と部署の組み合わせが間違っています");
		}

		if(branchId == 2 && (departmentId == 1 || departmentId == 2)) {
			errorMessages.add("支社と部署の組み合わせが間違っています");
		}

		if(differentUser != null && differentUserId != id) {
			errorMessages.add("アカウントが重複しています");
		}

		if (errorMessages.size() != 0) {
			return false;
		}
		return true;
	}

}
