package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import beans.Branch;
import beans.Department;
import beans.User;
import service.BranchService;
import service.DepartmentService;
import service.UserService;


@WebServlet(urlPatterns = {"/signup"})
public class SignUpServlet extends HttpServlet{

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
		throws IOException, ServletException{

		List<Branch> branches = new BranchService().select();
		request.setAttribute("branches", branches);
		List<Department> departments = new DepartmentService().select();
		request.setAttribute("departments", departments);

		request.getRequestDispatcher("signup.jsp").forward(request,response);
	}

	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException{
		List<Branch> branches = new BranchService().select();
		List<Department> departments = new DepartmentService().select();

		List<String> errorMessages = new ArrayList<String>();

		String account = request.getParameter("account");
		String name = request.getParameter("name");
		int branchId = Integer.parseInt(request.getParameter("branchId"));
		int departmentId = Integer.parseInt(request.getParameter("departmentId"));

		User user = getUser(request);
		if (!isValid(request, errorMessages)) {

			request.setAttribute("branches", branches);
			request.setAttribute("departments", departments);
			request.setAttribute("name", name );
			request.setAttribute("account", account );
			request.setAttribute("branchId", branchId );
			request.setAttribute("departmentId", departmentId );
			request.setAttribute("errorMessages", errorMessages);
			request.getRequestDispatcher("signup.jsp").forward(request, response);
			return;
		}

		new UserService().insert(user);
		response.sendRedirect("./");

	}
	private User getUser(HttpServletRequest request)
			throws IOException, ServletException{

		User user = new User();
		user.setAccount(request.getParameter("account"));
		user.setPassword(request.getParameter("password"));
		user.setName(request.getParameter("name"));
		user.setBranchId(Integer.parseInt(request.getParameter("branchId")));
		user.setDepartmentId(Integer.parseInt(request.getParameter("departmentId")));
		return user;
	}

	private boolean isValid(HttpServletRequest request, List<String> errorMessages) {
		String account = request.getParameter("account");
		String password = request.getParameter("password");
		String passwordConfirm = request.getParameter("passwordConfirm");
		String name = request.getParameter("name");
		int branchId = Integer.parseInt(request.getParameter("branchId"));
		int departmentId = Integer.parseInt(request.getParameter("departmentId"));

		User differentUser = new UserService().select(account);

		if(StringUtils.isEmpty(account)) {
			errorMessages.add("アカウント名を入力してください");
		}

		if(account.length() > 20) {
			errorMessages.add("アカウントは20文字以下で入力してください");
		}

		if(account.length() < 6) {
			errorMessages.add("アカウントは6文字以上で入力してください");
		}

		if(!account.matches("^[0-9a-zA-Z]+$")) {
			errorMessages.add("アカウントに使用できない文字が含まれています");
		}

		if(StringUtils.isEmpty(password)) {
			errorMessages.add("パスワードを入力してください");
		}

		if(!password.matches("^[-_@+*;:#$%&A-Za-z0-9]+$")) {
			errorMessages.add("パスワードに使用できない文字が含まれています");
		}

		if(password.length() < 6) {
			errorMessages.add("パスワードは6文字以上で入力してください");
		}

		if(password.length() > 20) {
			errorMessages.add("パスワードは20文字以下で入力してください");
		}

		if(StringUtils.isEmpty(name)) {
			errorMessages.add("名前を入力してください");
		}

		if(name.length() > 10) {
			errorMessages.add("名前は10文字以下で入力してください");
		}

		if(branchId == 2 && (departmentId == 1 || departmentId ==2)) {
			errorMessages.add("支社と部署の組み合わせが間違っています");
		}
		if(branchId == 4 && (departmentId == 1 || departmentId ==2)) {
			errorMessages.add("支社と部署の組み合わせが間違っています");
		}

		if(branchId == 1 && (departmentId == 3 || departmentId ==4)) {
			errorMessages.add("支社と部署の組み合わせが間違っています");
		}

		if(branchId == 2 && (departmentId == 1 || departmentId ==2)) {
			errorMessages.add("支社と部署の組み合わせが間違っています");
		}

		if(!password.equals(passwordConfirm)) {
			errorMessages.add("入力したパスワードと確認用パスワードが一致しません");
		}

		if(differentUser != null) {
			errorMessages.add("アカウントが重複しています");
		}

		if(errorMessages.size() != 0) {
			return false;
		}
		return true;
	}
}

