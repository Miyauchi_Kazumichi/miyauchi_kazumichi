package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.User;
import exception.NoRowsUpdatedRuntimeException;
import service.UserService;

@WebServlet(urlPatterns = {"/stop"})
public class StopServlet extends HttpServlet{

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		//jspで渡されたis_stoppedを受け取る
		User user = new User();
		user.setId(Integer.parseInt(request.getParameter("id")));
		user.setIsStopped(Integer.parseInt(request.getParameter("is_stopped")));

		try {
			new UserService().updateIsStopped(user);
		}catch (NoRowsUpdatedRuntimeException e) {
			return ;
		}

		response.sendRedirect("management");
	}


}
