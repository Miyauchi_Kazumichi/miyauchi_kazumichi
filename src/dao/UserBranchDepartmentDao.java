package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.UserBranchDepartment;
import exception.SQLRuntimeException;


public class UserBranchDepartmentDao {

	public List<UserBranchDepartment> select(Connection connection, int num) {
		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("    users.id, ");
			sql.append("    users.account, ");
			sql.append("    users.name, ");
			sql.append("    branches.name as branch_name, ");
			sql.append("    departments.name as department_name, ");
			sql.append("    users.is_stopped, ");
			sql.append("    users.created_date, ");
			sql.append("    users.branch_id, ");
			sql.append("    users.department_id ");
			sql.append("FROM users ");
			sql.append("INNER JOIN branches ");
			sql.append("ON users.branch_id = branches.id ");
			sql.append("INNER JOIN departments ");
			sql.append("ON users.department_id = departments.id ");
			sql.append("ORDER BY users.created_date DESC limit " + num);

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();

			List<UserBranchDepartment> usersList = toUserBranchDepartment(rs);
			return usersList;
			}catch (SQLException e) {
				throw new SQLRuntimeException(e);
			} finally {
				close(ps);
			}

	}

	private  List<UserBranchDepartment> toUserBranchDepartment(ResultSet rs) throws SQLException {

		List<UserBranchDepartment> usersList= new ArrayList<UserBranchDepartment>();
		try {
			while (rs.next()) {
				UserBranchDepartment userBranchDepartment = new UserBranchDepartment();
				userBranchDepartment.setId(rs.getInt("id"));
				userBranchDepartment.setAccount(rs.getString("account"));
				userBranchDepartment.setName(rs.getString("name"));
				userBranchDepartment.setBranchName(rs.getString("branch_name"));
				userBranchDepartment.setDepartmentName(rs.getString("department_name"));
				userBranchDepartment.setIsStopped(rs.getInt("is_stopped"));
				userBranchDepartment.setCreatedDate(rs.getTimestamp("created_date"));

				usersList.add(userBranchDepartment);
			}
			return usersList;
		} finally {
			close(rs);
		}
	}
}
