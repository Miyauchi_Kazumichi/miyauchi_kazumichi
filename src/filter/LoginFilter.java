package filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;



@WebFilter(urlPatterns ={"/message", "/index.jsp", "/setting", "/comment", "/management", "/signup"})
public class LoginFilter implements Filter{

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException,ServletException{
		System.out.println("ログインチェック");

		HttpSession session = ((HttpServletRequest) request).getSession();
		session.removeAttribute("errorMessages");
		User user = (User) session.getAttribute("loginUser");

		List<String> errorMessages = new ArrayList<String>();

		if(user == null){
			// セッションがNUllならば、ログイン画面へ飛ばす
			errorMessages.add("ログインしてください");
			session.setAttribute("errorMessages", errorMessages);
			((HttpServletResponse) response).sendRedirect("login");
			return;
		}

		chain.doFilter(request, response);

	}

	@Override
	public void init(FilterConfig config) throws ServletException{}

	@Override
	public void destroy(){}
}
