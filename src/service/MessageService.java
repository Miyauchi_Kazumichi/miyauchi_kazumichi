package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import beans.Message;
import beans.UserMessage;
//import dao.UserMessageDao;
import dao.MessageDao;
//import dao.UserMessageDao;
import dao.UserMessageDao;

public class MessageService {

	public void insert(Message message) {

		Connection connection = null;
		try {
			connection = getConnection();
			new MessageDao().insert(connection, message);
			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
	public List<UserMessage> select(String date1, String date2, String messageCategory) {
		final int LIMIT_NUM = 1000;

		String startDate = new String();
		String endDate = new String();
		String category = new String();

		Connection connection = null;
		try {
			connection = getConnection();
			if(!StringUtils.isEmpty(date1)) {
				startDate = date1 + " 00:00:00";
			}else {
				startDate = "2020-01-01 00:00:00";
			}

			if(!StringUtils.isEmpty(date2)) {
				endDate = date2 + " 23:59:59";
			}else {
				Date date = new Date();
				SimpleDateFormat sdFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
				endDate = sdFormat.format(date);
			}
			if(!StringUtils.isEmpty(messageCategory)) {
				category = messageCategory;
			}else {
				category.isEmpty();
			}
			List<UserMessage> messages = new UserMessageDao().select(connection, LIMIT_NUM, startDate, endDate, category);
			commit(connection);

			return messages;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public void delete(int messageId) {
		Connection connection = null;
		try {
			connection = getConnection();
			new MessageDao().delete(connection, messageId);
			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}

	}

}